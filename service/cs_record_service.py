from flask import request

from bmwgs.app import db
from bmwgs.models.car_model_keyword_cs_record import CarModelKeywordCsRecord
from bmwgs.models.customer_service_record import CustomerServiceRecord


def get_cs_record_cnt_between_cs_start_date_by_keyword_key(start_date, end_date, keyword_key):
    """
    키워드에 해당 하는 날짜사이의 CS_RECORD 값을 반환한다.
    """
    page = request.args.get('page', type=int)
    per_page = request.args.get('per-page', type=int)

    return db.session.query(CustomerServiceRecord.cs_start_date, CustomerServiceRecord.contents) \
        .join(CarModelKeywordCsRecord, CarModelKeywordCsRecord.customer_service_record_id == CustomerServiceRecord.id) \
        .filter(CarModelKeywordCsRecord.car_model_keyword_id == keyword_key) \
        .filter(CustomerServiceRecord.cs_start_date.between(start_date, end_date)) \
        .order_by(CustomerServiceRecord.cs_start_date.desc()) \
        .paginate(page, per_page=per_page)
