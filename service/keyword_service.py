from sqlalchemy import func

from bmwgs.app import db
from bmwgs.models.car_model_keyword_cs_record import CarModelKeywordCsRecord
from bmwgs.models.category import CustomerServiceCategory
from bmwgs.models.customer_service_record import CustomerServiceRecord
from bmwgs.models.keyword import Keyword


def get_keyword_rank_list_between_date(start_date, end_date, limit):
    return db.session.query(Keyword.id, Keyword.keyword, func.sum(CarModelKeywordCsRecord.count).label('total'), CustomerServiceCategory.category_name) \
        .join(CarModelKeywordCsRecord, CarModelKeywordCsRecord.car_model_keyword_id == Keyword.id) \
        .join(CustomerServiceRecord, CustomerServiceRecord.id == CarModelKeywordCsRecord.customer_service_record_id) \
        .join(CustomerServiceCategory, CustomerServiceCategory.id == CustomerServiceRecord.cs_category_id) \
        .filter(CustomerServiceRecord.cs_start_date.between(start_date, end_date)) \
        .group_by(Keyword.id) \
        .order_by(func.sum(CarModelKeywordCsRecord.count).desc()) \
        .limit(limit) \
        .all()
