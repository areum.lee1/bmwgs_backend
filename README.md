# bmwgs_backend

## 사용법

### 아키텍처 문서

  [구글ppt](https://docs.google.com/presentation/d/1d3-EZzOpRukpOM-PfWu_W9IKxQAGGrSEgEFXLkHN7Co/edit#slide=id.gf2cd1668ab_1_6)

### Frontend App주소

  [프론트엔드 앱](http://ec2-3-37-174-60.ap-northeast-2.compute.amazonaws.com:30330/external/pageOne)

### Swagger Web주소

  [API스웨거](http://ec2-3-37-174-60.ap-northeast-2.compute.amazonaws.com:30001/bmwgs/api/v1.0/doc/)
  

### 설치와 로컬 실행

- python3 virtual environment를 만들고 활성화 하세요

      python3 -m venv venv
      source venv/bin/activate (mac)
      venv\Scripts\activate.bat (windows)

- requirements를 설치 하세요

      pip install -r bmwgs/requirements.txt

- Database 마이그레이션(migration)
    
  DB에 변경사항이 생기면 flask db migrate 명령어로 db에 반영 해줍니다.
  
      flask db init
  
      flask db migrate
  
      flask db upgrade

- Run development server

      python run.py


- View the Swagger API docs in your browser:

  http://localhost:5000/bmwgs/api/v1.0/doc/

### 배포된 주소


- Static code analysis - run this command:

      flake8

- Run unit tests

      pytest .

- Generate test case coverage

      coverage run -m pytest && coverage report --omit='*lib/*.py,*test_*.py'
      coverage xml -i

### Model고치는 법

- models의 class를 고치고

- schema고치고

- api쪽에도 고쳐야 반영됨


## Running on docker

- Build image and run

      docker build -t project:1 .
      docker run --name project --env-file .env -d -p 8000:8000 project:1

- View Swagger API docs in the browser

  http://localhost:8000/bmwgs/api/v1.0/doc/

- Run tests in the docker container

      docker exec -it project bash
      pytest .

## 참고

- restx
  
  https://flask-restx.readthedocs.io/en/latest/

## ARGO

  https://gitlab.com/areum.lee1/argo-bmw-apps
