FROM python:3.8-slim

#RUN apt-get update

RUN mkdir app
ADD . /app
WORKDIR /app
RUN pip3 install -r bmwgs/requirements.txt
EXPOSE 8000
ENTRYPOINT ["gunicorn", "-c", "gunicorn_config.py", "wsgi:app"]
