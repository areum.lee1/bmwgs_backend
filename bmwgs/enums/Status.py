from enum import Enum

class Status(Enum):
    COMPLETE = "COMPLETE"
    IN_PROGRESS = "IN PROGRESS"


# print(repr(Status.COMPLETE))