import json

from bmwgs.tests.conftest import AppTestCase


class bbsTestCase(AppTestCase):
    url = "bmwgs/api/v1.0/bbs"

    def test_create_bbs(self):
        resp = self.client.post(
            self.url,
            data=json.dumps({"name": "John Doe", "email": "john.doe@example.com"}),
            content_type="application/json",
        )
        self.assertEqual(resp.status_code, 201)

    def test_get_bbs_not_found(self):
        resp = self.client.get(self.url + "/10")
        self.assertEqual(resp.status_code, 404)

    def test_get_bbs(self):
        resp_create = self.client.post(
            self.url,
            data=json.dumps({"name": "John Doe", "email": "john.doe@example.com"}),
            content_type="application/json",
        )

        resp = self.client.get("{}/{}".format(self.url, resp_create.json["bbs"]["id"]))
        self.assertEqual(resp.status_code, 200)
