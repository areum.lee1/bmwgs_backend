from flask import request
from flask_restx import Resource, fields

from bmwgs.bbs import ns


bbs = ns.model(
    "bbs",
    {
        "id": fields.Integer,
    },
)


@ns.route("/bbs", methods=["GET", "POST"])
class bbsItem(Resource):
    @ns.expect(bbs, validate=True)
    @ns.marshal_with(bbs, envelope="bbs")
    def post(self):
        data = request.json
        data["id"] = 1
        return data, 201

    def get(self):
        return 'eee', 201

@ns.route("/bbs/<string:bbs_id>", methods=["GET"])
class bbsDetail(Resource):
    @ns.marshal_with(bbs, envelope="bbs")
    def get(self, bbs_id):
        if bbs_id == "1":
            return {"id": 1}
        return {}, 404
