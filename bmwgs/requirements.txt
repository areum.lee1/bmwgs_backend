Flask==1.0.2
Flask-Cors==3.0.10
flask-restx==0.2.0
Flask-SQLAlchemy==2.5.1
Flask-Migrate==3.1.0
Flask-Script==2.0.6

flask-marshmallow==0.14.0
marshmallow==3.13.0
marshmallow-sqlalchemy==0.26.1
konlpy==0.5.0
tweepy==3.10.0
# JPype1-py3==0.5.5.4 # gitlab에서 빌드 안돼서 뺌

gunicorn==19.8.1

# QA tools
flake8==3.5.0

#Testing
pytest==3.6.3
Flask-Testing==0.7.1
coverage==4.5.1

#MySQL
PyMySQL==1.0.2

# greenlet mac에서 필요함
greenlet==1.1.2