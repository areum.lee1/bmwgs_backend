from flask_restx import Resource, fields, reqparse
from flask import request, jsonify, make_response

from datetime import date

from bmwgs.statistics import ns
from bmwgs.models.customer_service_record import CustomerServiceRecord, CustomerServiceRecordSchema

statistics = ns.model(
    "Statistics",
    {
        "total_count": fields.Integer(required=True),
        "completion_rate": fields.Float(required=True),
        "completed_task": fields.Integer(required=True),
        "averate_time": fields.Integer(required=True),
        "daily_average_task": fields.Integer(required=True),
    },
)

parser = reqparse.RequestParser()
parser.add_argument("start", type=str, location="args")
parser.add_argument("end", type=str, location="args")


@ns.route("/")
class statisticsItem(Resource):
    @ns.marshal_with(statistics)
    @ns.expect(parser)
    def get(self):
        """통계 정보를 aggregation해서 내려줍니다."""
        start = parser.parse_args()["start"]
        end = parser.parse_args()["end"]

        total_count = CustomerServiceRecord.query.filter(
            CustomerServiceRecord.date.between(
                date.fromisoformat(start).strftime("%Y-%m-%d"),
                date.fromisoformat(end).strftime("%Y-%m-%d"),
            )
        ).count()

        print(total_count)
        return {}, 200

    @ns.doc("create_statistics")
    @ns.expect(statistics)
    @ns.marshal_list_with(statistics, code=201)
    def post(self):
        """통계 정보를 입력 합니다."""
        input = request.json
        category_schema = ()

        category = CustomerServiceRecord(
            category1_id=input["category1_id"],
            category1_name=input["category1_name"],
            category2_id=input["category2_id"],
            category2_name=input["category2_name"],
        )
        category.create()
        category.commit()
        output = category_schema.dump(category)
        return output, 200
