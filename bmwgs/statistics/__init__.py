from flask_restx import Namespace

ns = Namespace("Statistics", path="/statistics")


from bmwgs.statistics.v1 import views  # noqa
