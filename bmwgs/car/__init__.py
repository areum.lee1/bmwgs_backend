from flask_restx import Namespace

ns = Namespace("Car", path="/car")

from bmwgs.car.v1 import views  # noqa
