from flask import request
from flask_restx import fields
from flask_restx import reqparse
from flask_restx import Resource

from bmwgs.car import ns
from bmwgs.models.car import CarModel
from bmwgs.models.car import CarModelSchema

car_model_response_dto = ns.model(
    "Car",
    {
        "id": fields.Integer,
        "name": fields.String(required=True),
        "level": fields.Integer(required=True),
        "parent_id": fields.Integer(required=True),
    }
)

parser = reqparse.RequestParser()
car_model_schema = CarModelSchema()
car_models_schema = CarModelSchema(many=True)


@ns.route("/models/<int:model_id>", methods=["GET", "PUT", "DELETE"])
class GetCarModelDetails(Resource):
    update_car_model_request_dto = ns.model(
        "Car",
        {
            "name": fields.String(required=True),
            "level": fields.Integer(required=True),
            "parent_id": fields.Integer(required=True),
        }
    )

    """차량 모델 개별 조회"""
    @ns.doc("차량 모델 정보를 반환")
    @ns.marshal_list_with(car_model_response_dto)
    def get(self, model_id):
        """
        개별 차량 모델 정보를 반환한다.
        """
        car_model = car_models_schema.dump(
            CarModel.query.filter_by(id=model_id).all()
        )
        return car_model, 200


    @ns.doc("차량 모델 정보를 수정")
    @ns.marshal_list_with(car_model_response_dto)
    @ns.expect(update_car_model_request_dto)
    def put(self, model_id):
        """
        개별 차량 모델 정보를 수정한다.
        """
        params = request.json

        target_car_model = CarModel(
            name=params["name"],
            level=params["level"],
            parent_id=params["parent_id"]
        )

        source_car_model = CarModel.query.get(model_id)
        source_car_model.change_with(target_car_model)
        source_car_model.commit()

        return source_car_model, 200


    @ns.doc("차량 모델 정보를 삭제")
    @ns.marshal_list_with(car_model_response_dto, code=204)
    def delete(self, model_id):
        """
        개별 차량 모델 정보를 삭제한다.
        """
        source_car_model = CarModel.query.get(model_id)
        source_car_model.delete()


@ns.route("/models/", methods=["GET"])
class GetCarModelList(Resource):
    """차량 모델 목록을 조회 한다."""

    @ns.doc("get car model list")
    @ns.marshal_list_with(car_model_response_dto)
    def get(self):
        """모든 차량 모델 목록을 반환 한다."""
        root_car_models = CarModel.get_car_model_by_parent_is_null()
        car_models = car_models_schema.dump(
            root_car_models
        )
        return car_models, 200


@ns.route("/models", methods=["POST"])
class CreateCarModelDetail(Resource):
    """차량 모델을 생성 한다."""
    create_car_model_request_dto = ns.model(
        "Car",
        {
            "name": fields.String(required=True),
            "level": fields.Integer(required=True),
            "parent_id": fields.Integer(required=True),
        }
    )

    @ns.doc("차량 모델 정보를 생성")
    @ns.marshal_list_with(car_model_response_dto, code=201)
    @ns.expect(create_car_model_request_dto)
    def post(self):
        """
        차량 모델을 생성한다.
        """
        params = request.json

        car_model = CarModel(
            name=params["name"],
            level=params["level"],
            parent_id=params["parent_id"]
        )
        car_model.create()
        output = car_model_schema.dump(car_model)
        return output, 201
