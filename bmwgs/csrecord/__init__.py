from flask_restx import Namespace

ns = Namespace("CustomServiceRecord", path="/csrecord")


from bmwgs.csrecord.v1 import views  # noqa
