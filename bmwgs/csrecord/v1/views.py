from flask import jsonify
from flask import make_response
from flask import request
from flask_restx import fields
from flask_restx import reqparse
from flask_restx import Resource

from bmwgs.csrecord import ns
from bmwgs.models import customer_service_record
from bmwgs.models.category import CustomerServiceCategory
from bmwgs.models.customer_service_record import CustomerServiceRecord
from bmwgs.models.customer_service_record import CustomerServiceRecordSchema
from bmwgs.utils.AlchemyEncoder import AlchemyEncoder
from bmwgs.utils.utils import Utils
from service.cs_record_service import get_cs_record_cnt_between_cs_start_date_by_keyword_key

import json
from datetime import datetime

cs_record = ns.model(
    "CustomerServiceRecord",
    {
        "id": fields.Integer,
        "case_id": fields.Integer,
        "cs_assignee": fields.String,
        "status": fields.String,
        "cs_start_date": fields.DateTime
    },
)

parser = reqparse.RequestParser()
parser.add_argument("start", type=str, location="args", help='시작 날짜')
parser.add_argument("end", type=str, location="args", help='종료 날짜')

customer_service_record_schema = CustomerServiceRecordSchema()
customer_service_records_schema = CustomerServiceRecordSchema(many=True)

customer_service_record = CustomerServiceRecord()
cs_category = CustomerServiceCategory()


# @ns.route("/")
# class CreateCustomerServiceRecordList(Resource):
# @ns.doc("list_customer_service_record")
# # @ns.marshal_list_with(cs_record)
# def get(self):
#     """모든 새로운 CUSTOM_SERVICE_RECORD 생성 레코드 리스트를 조회"""
#     cs_records = customer_service_record.get_all_cs_records()
#     return customer_service_records_schema.dump(cs_records)

# @ns.doc("create_customer_service_record")
# @ns.expect(cs_record)
# @ns.marshal_with(cs_record, code=201)
# def post(self):
#     """새로운 CUSTOM_SERVICE_RECORD 생성"""
#     params = request.json
#     new_customer_service_record = CustomerServiceRecord.value_of(
#         case_id=params["case_id"],
#         cs_assignee=params["cs_assignee"],
#         status=params["status"],
#         cs_start_date=["cs_start_date"]
#     )
#     new_customer_service_record.create()
#     return new_customer_service_record


@ns.route("/statistic/")
class GetCustomerServiceStatistic(Resource):
    """통계 정보를 반환한다."""

    @ns.expect(parser)
    @ns.doc("get customer service statistics record list")
    def get(self):
        """ CUSTOMER SERVICE 통계 정보"""
        params = request.args
        start_date = params['start']
        end_date = params['end']

        # 상담 완료율
        cs_complete_rate = customer_service_record.get_cs_complete_rate_between_date(start_date=start_date,
                                                                                     end_date=end_date)
        # 일 평균 상담 건수
        average_cs_count_per_day = customer_service_record.get_average_cs_count_per_day(start_date=start_date,
                                                                                        end_date=end_date)
        # 상담 처리 건수
        complete_cs_cnt = customer_service_record.get_complete_cs_cnt_between_date(start_date=start_date,
                                                                                   end_date=end_date)
        # 총 상담 건수
        total_cs_num = customer_service_record.get_total_cs_cnt_between_date(start_date=start_date, end_date=end_date)

        response_dto = {
            'cs_complete_rate': cs_complete_rate,
            'average_cs_count_per_day': average_cs_count_per_day,
            'complete_cs_cnt': complete_cs_cnt,
            'total_cs_num': total_cs_num
        }
        return make_response(jsonify(response_dto), 200)

def category_map(category_list):
    m = {}
    for t in category_list:
        if m.get(t[0]) == None:
            m[t[0]] = {"category_name":t[1], "parent_id":t[2]}
    return m


@ns.route("/chart-info/cs-record-cnt-by-category")
class GetCustomerServiceStatistic(Resource):

    @ns.expect(parser)
    @ns.doc("대분류 카테고리")
    def get(self):
        """ 대분류 카테고리별 차트 정보(Current Isseue) # 1-1"""
        params = request.args
        start_date = params['start']
        end_date = params['end']

        # 모든 카테고리를 map으로 만듬
        m = category_map(cs_category.get_all())
        each_category_count = customer_service_record.category_cnt(start_date, end_date)
        print('each_category_count', each_category_count)
        # 각 parent category별로 개수가 몇개인지 세는 로직
        r2 = {}
        for t in each_category_count:
            category_info = m.get(t[0])
            if category_info != None:
                if r2.get(category_info['parent_id']) == None:
                    r2[category_info['parent_id']] = t[1]
                r2[category_info['parent_id']] += t[1]

        ret = []
        for parent_category in r2.items():
            jo = {"category_id":parent_category[0], 'count':parent_category[1], 'category_name':m[parent_category[0]]['category_name']}
            ret.append(jo)
        print('ret', ret)
        response_dto = {
            'data': ret
        }

        return make_response(jsonify(response_dto), 200)


@ns.route("/chart-info/cs-record-cnt-by-category/<int:root_category_key>")
class GetCustomerServiceStatistic(Resource):

    @ns.expect(parser)
    @ns.doc("중분류 카테고리별 차트 정보")
    def get(self, root_category_key):
        """ 중분류 카테고리별 차트 정보"""
        params = request.args
        start_date = params['start']
        end_date = params['end']

        child_categories = cs_category.get_child_category_from_root_category_key(root_category_key)

        ret = []
        for child_category in child_categories:
            category_key = child_category.id
            cs_records_cnt = customer_service_record.get_cs_record_cnt_between_cs_start_date_by_category_key(start_date,
                                                                                                             end_date,
                                                                                                             category_key)
            ret.append({
                'category_name': child_category.category_name,
                'count': cs_records_cnt,
                'category_id': category_key
            })

        response_dto = {
            'data': ret
        }

        return make_response(jsonify(response_dto), 200)


@ns.route("/chart-info/cs-record-cnt-diff")
class GetCustomerServiceStatistic(Resource):

    @ns.expect(parser)
    @ns.doc("상담건수 추이 비교차트")
    def get(self):
        """ 상담 건수 추이 비교 차트 데이터 # 1-3"""
        params = request.args
        start_date = params['start']
        end_date = params['end']

        # 조회할 데이터 (시작일, 종료일)
        cs_records_cnt_list = customer_service_record.get_cnt_group_by_cs_start_date(start_date,
                                                                                     end_date)

        diff_day_dict = Utils.generate_diff_days_dict(start_date, end_date)

        for cs_records_cnt in cs_records_cnt_list:
            date, cnt = cs_records_cnt
            date = Utils.date_to_str(date)
            diff_day_dict[date] = cnt

        # 비교할 데이터 (시작일 - 7, 종료일 - 7)
        before_7days_start_date = Utils.get_str_date_days_before(start_date, 7)
        before_7days_end_date = Utils.get_str_date_days_before(end_date, 7)
        cs_records_cnt_list2 = customer_service_record.get_cnt_group_by_cs_start_date(before_7days_start_date,
                                                                                      before_7days_end_date)

        diff_day_dict2 = Utils.generate_diff_days_dict(before_7days_start_date, before_7days_end_date)

        for cs_records_cnt in cs_records_cnt_list2:
            date, cnt = cs_records_cnt
            date = Utils.date_to_str(date)
            diff_day_dict2[date] = cnt

        date_list = []
        cs_record_cnt_by_date = []
        cs_record_cnt_before_7days = []

        for key in diff_day_dict.keys():
            date_list.append(key)
            cs_record_cnt_by_date.append(diff_day_dict[key])

        for key in diff_day_dict2.keys():
            cs_record_cnt_before_7days.append(diff_day_dict2[key])

        response_dto = {
            'data': {
                'x_axis': date_list,
                'y_axis1': cs_record_cnt_by_date,
                'y_axis2': cs_record_cnt_before_7days
            }
        }

        return make_response(jsonify(response_dto), 200)


@ns.route("/<int:keyword_key>/keyword")
class GetCustomerServiceRecord(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("start-date", type=str, location="args", help='시작 날짜')
    parser.add_argument("end-date", type=str, location="args", help='종료 날짜')
    parser.add_argument("page", type=int, location="args", default=1, help='페이지 번호')
    parser.add_argument("per-page", type=int, location="args", default=5, help='불러올 개수')

    @ns.expect(parser)
    @ns.doc("해당 키워드에 속하는 상담 데이터를 반환한다.")
    def get(self, keyword_key):
        """ 해당 키워드에 속하는 상담 데이터를 반환한다."""
        start_date = request.args.get('start-date', type=str)
        end_date = request.args.get('end-date', type=str)

        page_obj = get_cs_record_cnt_between_cs_start_date_by_keyword_key(start_date, end_date, keyword_key)

        cs_records = page_obj.items
        page_info = {
            'total_cnt': page_obj.total,
            'has_next': page_obj.has_next,
            'has_prev': page_obj.has_prev,
            'prev_num': page_obj.prev_num,
            'next_num': page_obj.next_num,
            'per_page': page_obj.per_page,
            'current_page': page_obj.page,
            'page_list': page_obj.pages
        }

        ret = []
        for cs_record in cs_records:
            date_and_time, contents = cs_record
            date, time = Utils.split_date_and_time_str_from_datetime(date_and_time)
            ret.append(
                {
                    'date': date,
                    'time': time,
                    'contents': contents
                }
            )

        response_dto = {
            'data': ret,
            'page_info': page_info
        }

        return make_response(jsonify(response_dto), 200)

# @ns.route("/<int:id>")
# @ns.response(404, "Customer Service Record not found")
# @ns.param("id", "The customer service identifier")
# class CustomerServiceRecordItem(Resource):
#     """Show a single customer service record item and lets you delete them"""
#
#     @ns.doc("get_customer_service_record")
#     @ns.marshal_with(cs_record)
#     def get(self, id):
#         """Fetch a customer service record given id"""
#         cs_record = CustomerServiceRecord.query.first_or_404(id=1)
#         return customer_service_record_schema.dump(cs_record)
#
#     @ns.doc("delete_customer_service_record")
#     @ns.response(204, "Customer Service Record is deleted")
#     def delete(seld, id):
#         """Delete a customer service record given id"""
#         cs_record = CustomerServiceRecord.query.filter_by(id=id)
#         cs_record.delete()
#         cs_record.commit()
#         return "", 204
#
#     @ns.doc("update_customer_service_record")
#     @ns.marshal_with(cs_record)
#     def put(self, id):
#         """Update a customer service record given its identifier"""
#         input = request.json
#
#         cs_record = CustomerServiceRecord.query.filter_by(id=id)
#         cs_record.category1 = input["category1"]
#         cs_record.category2 = input["category2"]
#         cs_record.case_id = input["case_id"]
#         cs_record.representative_name = input["representative_name"]
#         cs_record.car_model_name = input["car_model_name"]
#         cs_record.date = input["date"]
#         cs_record.result = input["result"]
#
#         return CustomerServiceRecord.dump(cs_record), 200
