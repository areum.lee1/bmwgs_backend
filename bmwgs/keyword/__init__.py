from flask_restx import Namespace

ns = Namespace("Keyword", path="/keyword")


from bmwgs.keyword.v1 import views  # noqa
