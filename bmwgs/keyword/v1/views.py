from datetime import timedelta

from flask import jsonify
from flask import make_response
from flask import request
from flask_restx import fields
from flask_restx import reqparse
from flask_restx import Resource

from bmwgs.keyword import ns
from bmwgs.models.customer_service_record import CustomerServiceRecord
from bmwgs.models.keyword import Keyword
from bmwgs.models.keyword import KeywordSchema
from service.keyword_service import get_keyword_rank_list_between_date

keyword = ns.model(
    "Keyword",
    {
        "id": fields.Integer,
        "keyword": fields.String(required=True),
        "emotion_category": fields.String(required=True),
        "count": fields.Integer(required=True),
    },
)

cs_record = CustomerServiceRecord()
parser = reqparse.RequestParser()

keywords_schema = KeywordSchema(many=True)

# 3-1
@ns.route("/")
class KeywordList(Resource):
    """Get a list of all customer service record, and POST a new customer service record"""

    @ns.doc("list_keyword_service")
    @ns.marshal_list_with(keyword)
    def get(self):
        """모든 keyword목록 보기 # 3-1"""
        keywords = keywords_schema.dump(
            Keyword.query.all()
        )
        return keywords

    @ns.doc("create_keyword")
    @ns.expect(keyword)
    @ns.marshal_list_with(keyword, code=201)
    def post(self):
        """새로운 키워드 정보 입력"""
        input = request.json
        keyword = Keyword(
            representative_name=input["representative_name"],
            car_model_name=input["car_model_name"],
            date=input["date"],
            time=timedelta(seconds=input["time"]),
            result=input["result"]
        )
        keyword.create()
        keyword.commit()
        output = keywords_schema.dump(keyword)
        return output, 200

# 3-2
@ns.route("/car_model/<int:car_model_id>/")
class GetKeywordListByCarModelId(Resource):
    """Keyword에 관련된 API."""

    @ns.doc("차량 모델에 따른 키워드 리스트를 반환한다.")
    def get(self, car_model_id):
        """차량 모델에 따른 키워드 리스트를 반환한다. #3-2 """
        keyword_list_limit_50 = Keyword.get_keyword_list_by_car_model_id(car_model_id, limit=50)

        response = []
        for keyword in keyword_list_limit_50:
            keyword_id, keyword, count = keyword

            response.append({
                'id': keyword_id,
                'keyword': keyword,
                'count': int(count)
            })

        return make_response(jsonify(response), 200)


@ns.route("/rank")
class KeywordRank(Resource):
    parser.add_argument("start-date", type=str, location="args", help='시작 날짜')
    parser.add_argument("end-date", type=str, location="args", help='종료 날짜')
    @ns.doc("빈도수에 따른 순위별 키워드")
    @ns.expect(parser)
    def get(self):
        """빈도수에 따른 순위별 키워드"""
        params = request.args
        start_date = params['start-date']
        end_date = params['end-date']

        keyword_rank_list = get_keyword_rank_list_between_date(start_date=start_date, end_date=end_date, limit=10)

        ret = []
        for idx, keyword in enumerate(keyword_rank_list, start=1):
            ret.append({
                'rank': idx,
                'cnt': int(keyword.total),
                'keyword_key': keyword.id,
                'keyword': keyword.keyword,
                'case_name': keyword.category_name
            })

        response_dto = {
            'data': ret
        }
        return make_response(jsonify(response_dto), 200)


@ns.route("/wordcloud")
class Wordcloud(Resource):
    """워드클라우드 데이터"""

    @ns.doc("list_keyword_service")
    def get(self):
        """모든 keyword목록 보기"""
        return {"가능하다":10, "좋다":20}
