from flask import Flask

from flask_restx.apidoc import apidoc
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_cors import CORS

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()

ROOT_URL = "/bmwgs"


def create_app(config_name):
    from bmwgs.config import app_config

    app = Flask(__name__)
    CORS(app)
    app.config.from_object(app_config[config_name])
    app.config["APPLICATION_ROOT"] = ROOT_URL
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

    db.init_app(app)
    migrate.init_app(app, db)

    from bmwgs import models

    # Flask restplus uses apidoc to generate URLs for static files in swagger.
    #
    # If any of the follwing is true:
    #   1. apidoc is not registered as a blueprint (default use case)
    #   2. apidoc is registered after another blueprint,
    # restplus auto-registers the default apidoc at '/'.
    #
    # Hence, the `apidoc` should be the first blueprint registered. And it is
    # mounted at ROOT_URL
    # app.register_blueprint(apidoc, url_prefix=ROOT_URL)

    with app.app_context():
        from bmwgs.api_v1 import blueprint as api
        from bmwgs.healthcheck import healthcheck

        app.register_blueprint(api, url_prefix=ROOT_URL + "/api/v1.0")
        app.register_blueprint(healthcheck, url_prefix=ROOT_URL + "/version")
    return app
