from datetime import datetime
from datetime import timezone

from marshmallow import fields

from bmwgs.app import db
from bmwgs.app import ma


class CarModel(db.Model):
    __tablename__ = "car_model"
    __table_args__ = {"mysql_engine": "InnoDB"}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    level = db.Column(db.Integer, nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('car_model.id'), nullable=True)

    car_model_keyword = db.relationship("Keyword")
    customer_service_record = db.relationship("CustomerServiceRecord")
    children = db.relationship("CarModel", cascade='all, delete-orphan', backref=db.backref('child', remote_side=[id]))
    parents = db.relationship("CarModel", cascade='all', backref=db.backref("back", remote_side=[id]))

    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.now(timezone.utc), nullable=False)

    def create(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def commit(self):
        db.session.commit()

    def change_with(self, source):
        self.name = source.name
        self.level = source.level
        self.parent_id = source.parent_id

    @staticmethod
    def get_car_model_list():
        return db.session \
            .query(CarModel) \
            .filter(CarModel.level == 1) \
            .all()

    @staticmethod
    def get_parents_list(car_model_id):
        beginning_getter = db.session \
            .query(CarModel) \
            .filter(CarModel.id == car_model_id) \
            .cte(name='parent_for', recursive=True)
        with_recursive = beginning_getter \
            .union(db.session
                   .query(CarModel)
                   .filter(CarModel.id == beginning_getter.c.parent_id))
        ret = db.session.query(with_recursive).all()
        return ret

    @staticmethod
    def get_children_list(car_model_id):
        beginning_getter = db.session \
            .query(CarModel) \
            .filter(CarModel.id == car_model_id) \
            .cte(name='children_for', recursive=True)
        with_recursive = beginning_getter.union_all(
            db.session.query(CarModel).filter(CarModel.parent_id == beginning_getter.c.id)
        )
        return db.session.query(with_recursive).all()

    @staticmethod
    def get_car_model_by_parent_is_null():
        return db.session \
            .query(CarModel) \
            .filter(CarModel.parent_id == None) \
            .all()


def __init__(self, name, level, parent_id):
    self.name = name
    self.level = level
    self.parent_id = parent_id


def __repr__(self):
    return "<Categories : {}>".format(self.id)


class CarModelSchema(ma.SQLAlchemySchema):
    class Meta:
        model = CarModel

    id = fields.Number(dump_only=True)
    name = fields.String(required=True)
    level = fields.Number(required=True)
    parent_id = fields.Number(required=True)
    children = fields.Nested('CarModelSchema', many=True, exclude=("parent_id",))
