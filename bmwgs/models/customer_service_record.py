from datetime import datetime
from datetime import timezone

from sqlalchemy import func

from bmwgs.app import db
from bmwgs.app import ma
from bmwgs.models.keyword import Keyword


class CustomerServiceRecord(db.Model):
    __tablename__ = "customer_service_record"
    __table_args__ = {"mysql_engine": "InnoDB"}

    id = db.Column(db.Integer, primary_key=True)
    case_id = db.Column(db.Integer, unique=True, nullable=True)
    cs_assignee = db.Column(db.String(255), nullable=False)
    status = db.Column(db.String(255), nullable=False)
    cs_start_date = db.Column(db.DateTime, nullable=False)
    contents = db.Column(db.Text, nullable=True)

    car_model_id = db.Column(db.Integer, db.ForeignKey('car_model.id'))
    cs_category_id = db.Column(db.Integer, db.ForeignKey('customer_service_category.id'))

    car_model_keywords = db.relationship(Keyword, secondary="car_model_keyword_cs_record", backref="customer_service_record")
    category = db.relationship('CustomerServiceCategory')

    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.now(timezone.utc), nullable=False)

    def create(self):
        db.session.add(self)
        db.session.commit()

    def delete(self, user):
        db.session.delete(user)
        db.session.commit()

    def commit(self):
        db.session.commit()

    def get_all_cs_records(self):
        return db.session.query(CustomerServiceRecord).all()

    def get_cs_records_between_cs_start_date(self, start_date, end_date):
        """
        Current Isseue 차트 데이터를 만들 때 쓰는 쿼리
        """
        return db.session.query(CustomerServiceRecord).filter(
            CustomerServiceRecord.cs_start_date.between(start_date, end_date)
        ).all()

    def category_cnt(self, start_date, end_date):
        """
        Current Isseue 차트 데이터를 만들 때 쓰는 쿼리
        """
        return db.session.query(CustomerServiceRecord.cs_category_id, func.count(CustomerServiceRecord.cs_category_id).label('cnt')).filter(
            CustomerServiceRecord.cs_start_date.between(start_date, end_date)
        ).group_by(CustomerServiceRecord.cs_category_id).all()

    def get_cs_record_cnt_between_cs_start_date_by_category_key(self, start_date, end_date, category_id):
        """
        날짜와 특정 카테고리에 속하는 CS_RECORD의 개수를 반환한다.
        """
        return db.session.query(CustomerServiceRecord) \
            .filter_by(cs_category_id=category_id) \
            .filter(CustomerServiceRecord.cs_start_date.between(start_date, end_date)) \
            .count()

    def get_cnt_group_by_cs_start_date(self, start_date, end_date):
        return db.session.query(CustomerServiceRecord.cs_start_date,
                                func.count(CustomerServiceRecord.cs_start_date).label('total_cnt')) \
            .filter(CustomerServiceRecord.cs_start_date.between(start_date, end_date)) \
            .group_by(CustomerServiceRecord.cs_start_date).all()

    def get_total_cs_cnt_between_date(self, start_date, end_date):
        return db.session.query(CustomerServiceRecord).filter(
            CustomerServiceRecord.cs_start_date.between(start_date, end_date)
        ).count()

    def get_complete_cs_cnt_between_date(self, start_date, end_date):
        return db.session.query(CustomerServiceRecord) \
            .filter(CustomerServiceRecord.cs_start_date.between(start_date, end_date)) \
            .filter(CustomerServiceRecord.status == "COMPLETE").count()

    def get_cs_complete_rate_between_date(self, start_date, end_date):
        total_cs_cnt = self.get_total_cs_cnt_between_date(start_date, end_date)
        complete_cs_cnt = self.get_complete_cs_cnt_between_date(start_date, end_date)
        if not total_cs_cnt:
            return 0
        return (complete_cs_cnt / total_cs_cnt) * 100

    def get_average_cs_count_per_day(self, start_date, end_date):
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
        diff_days = (end_date - start_date).days + 1

        total_cs_cnt = self.get_total_cs_cnt_between_date(start_date, end_date)
        if diff_days != 0:
            return total_cs_cnt / diff_days
        else:
            return 0

    @classmethod
    def value_of(cls, cs_assignee, status, cs_start_date):
        return cls(cs_assignee=cs_assignee,
                   status=status,
                   cs_start_date=cs_start_date)

    def __repr__(self):
        return "<CustomerServiceRecord : {}>".format(self.id)


class CustomerServiceRecordSchema(ma.SQLAlchemySchema):
    class Meta:
        model = CustomerServiceRecord

    id = db.Column(db.Integer, primary_key=True)
    case_id = db.Column(db.Integer, unique=True, nullable=False)
    cs_assignee = db.Column(db.String(255), nullable=False)
    status = db.Column(db.String(255), nullable=False)
    contents = db.Column(db.Text, nullable=True)
    cs_start_date = db.Column(db.DateTime, nullable=False)
