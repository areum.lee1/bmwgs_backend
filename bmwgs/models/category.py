from datetime import datetime
from datetime import timezone

from marshmallow import fields

from bmwgs.app import db
from bmwgs.app import ma


class CustomerServiceCategory(db.Model):
    __tablename__ = "customer_service_category"
    __table_args__ = {"mysql_engine": "InnoDB"}

    id = db.Column(db.Integer, primary_key=True)
    category_name = db.Column(db.String(255), nullable=False)
    level = db.Column(db.Integer, nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('customer_service_category.id'), nullable=True)

    customer_service_record = db.relationship("CustomerServiceRecord")

    children = db.relationship("CustomerServiceCategory", cascade='all, delete-orphan',
                               backref=db.backref('parent', remote_side=[id]))

    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.now(timezone.utc), nullable=False)

    def create(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def commit(self):
        db.session.commit()

    def change_with(self, source):
        self.category_name = source.category_name
        self.level = source.level
        self.parent_id = source.parent_id
        db.session.commit()

    def get_all(self):
        return db.session \
            .query(CustomerServiceCategory.id, CustomerServiceCategory.category_name, CustomerServiceCategory.parent_id) \
            .all()

    def get_child_category_from_root_category_key(self, category_key):
        """
        부모 카테고리에 속한 자식 카테고리를 반환한다.
        """
        root_category = db.session \
            .query(CustomerServiceCategory)\
            .filter(CustomerServiceCategory.id == category_key)\
            .one()
        child_category = root_category.children

        return child_category

    @staticmethod
    def get_customer_service_category_by_parent_is_null():
        return db.session \
            .query(CustomerServiceCategory) \
            .filter(CustomerServiceCategory.parent_id == None) \
            .all()

    @staticmethod
    def get_customer_service_category_by_id(category_id):
        return db.session \
            .query(CustomerServiceCategory) \
            .filter(CustomerServiceCategory.id == category_id) \
            .all()


def __init__(self, category_name, level, parent_id):
    self.category_name = category_name
    self.level = level
    self.parent_id = parent_id


def __repr__(self):
    return "<Categories : {}>".format(self.id)


class CustomerServiceCategorySchema(ma.SQLAlchemySchema):
    class Meta:
        model = CustomerServiceCategory

    id = fields.Number(dump_only=True)
    category_name = fields.String(required=True)
    level = fields.Number(required=True)
    parent_id = fields.Number(required=True)
    children = fields.Nested('CustomerServiceCategorySchema', many=True, exclude=("parent_id",))
