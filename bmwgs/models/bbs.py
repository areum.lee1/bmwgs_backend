from bmwgs.app import db
from bmwgs.app import ma

from marshmallow import fields

class Bbs(db.Model):
    __tablename__ = "bbs"
    __table_args__ = {"mysql_engine": "InnoDB"}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10), unique=True, nullable=False)
    email = db.Column(db.String(30), unique=True, nullable=False)
    phone_no = db.Column(db.String(30), unique=True, nullable=True)

    def create(self):
        db.session.add(self)
        db.session.commit()

    def delete(self, user):
        db.session.delete(user)
        db.session.commit()

    def __init__(self, name, email):
        self.name = name
        self.email = email

    def __repr__(self):
        return "<Bbs : {}>".format(self.id)


class BbsSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Bbs

    id = fields.Number(dump_only=True)
    name = fields.String(required=True)
    email = fields.String(required=True)
    phone_no = fields.String(required=False)
