from datetime import datetime
from datetime import timezone

from marshmallow import fields
from sqlalchemy import func

from bmwgs.app import db
from bmwgs.app import ma
from bmwgs.models.car_model_keyword_cs_record import CarModelKeywordCsRecord

class Keyword(db.Model):
    __tablename__ = "car_model_keyword"
    __table_args__ = {"mysql_engine": "InnoDB"}

    id = db.Column(db.Integer, primary_key=True)
    keyword = db.Column(db.String(16), nullable=False)
    emotion_category = db.Column(db.String(16), nullable=False)
    count = db.Column(db.Integer, nullable=False)
    car_model_id = db.Column(db.Integer, db.ForeignKey('car_model.id'))

    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.now(timezone.utc), nullable=False)

    cs_records = db.relationship("CustomerServiceRecord", secondary="car_model_keyword_cs_record", backref="car_model_keyword")

    def create(self):
        db.session.add(self)
        db.session.commit()

    def delete(self, keyword):
        db.session.delete(keyword)
        db.session.commit()

    def commit(self):
        db.session.commit()

    @staticmethod
    def get_keyword_list_by_car_model_id(car_model_id, limit):
        return db.session.query(Keyword.id, Keyword.keyword, func.sum(CarModelKeywordCsRecord.count)) \
            .join(CarModelKeywordCsRecord, CarModelKeywordCsRecord.car_model_keyword_id == Keyword.id) \
            .filter(Keyword.car_model_id == car_model_id) \
            .group_by(Keyword.id) \
            .order_by(func.sum(CarModelKeywordCsRecord.count).desc()) \
            .limit(limit) \
            .all()

    def __init__(self, keyword, emotion_category, count):
        self.keyword = keyword
        self.emotion_category = emotion_category
        self.count = count

    def __repr__(self):
        return "<Keywords : {}>".format(self.id)


class KeywordSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Keyword

    id = fields.Number(dump_only=True)
    keyword = fields.String(required=True)
    emotion_category = fields.String(required=True)
    count = fields.Number(required=False)
