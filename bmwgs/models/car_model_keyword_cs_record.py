from datetime import datetime
from datetime import timezone

from bmwgs.app import db


class CarModelKeywordCsRecord(db.Model):
    __tablename__ = "car_model_keyword_cs_record"
    __table_args__ = {"mysql_engine": "InnoDB"}

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    car_model_keyword_id = db.Column(db.Integer, db.ForeignKey('car_model_keyword.id'), primary_key=True)
    customer_service_record_id = db.Column(db.Integer, db.ForeignKey('customer_service_record.id'), primary_key=True)
    count = db.Column(db.Integer, nullable=False)

    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.now(timezone.utc), nullable=False)
