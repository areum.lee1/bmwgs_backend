from datetime import datetime
from datetime import timedelta


class Utils:

    @classmethod
    def generate_diff_days_dict(cls, start_date, end_date):
        """
        시작 날짜와 종료 날짜 사이의 키 값이 날짜인 딕셔너리를 반환한다.
        """
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.strptime(end_date, "%Y-%m-%d")

        diff_day_list = [(start_date + timedelta(days=x)).strftime("%Y-%m-%d")
                for x in range((end_date - start_date).days + 1)]

        return dict.fromkeys(diff_day_list, 0)

    @classmethod
    def date_to_str(cls, date):
        """
        날짜 객체를 문자열로 변경하여 반환한다.
        """
        return date.strftime("%Y-%m-%d")

    @classmethod
    def get_str_date_days_before(cls, date, day):
        """
        입력한 날만큼 이전의 날짜를 반환한다.
        """
        date = datetime.strptime(date, "%Y-%m-%d") - timedelta(days=day)
        return date.strftime("%Y-%m-%d")

    @classmethod
    def split_date_and_time_str_from_datetime(cls, date_and_time: datetime):
        """
        DATETIME 자료형에서 날짜와 시간을 분리하여 문자열 형태로 반환한다.
        """
        date = [str(date_and_time.year), str(date_and_time.month), str(date_and_time.day)]
        time = [str(date_and_time.hour), str(date_and_time.minute), str(date_and_time.second)]

        date = '-'.join(date)
        time = ':'.join(time)

        return date, time
