from flask_restx import Namespace

ns = Namespace("User", path="/")


from bmwgs.user.v1 import views  # noqa
