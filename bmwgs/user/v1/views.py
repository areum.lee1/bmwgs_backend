from flask_restx import Resource, fields, reqparse
from flask import request, jsonify, make_response


from bmwgs.user import ns
from bmwgs.app import db
from bmwgs.models.user import User, UserSchema

user = ns.model(
    "User",
    {
        "id": fields.Integer,
        "name": fields.String(required=True, min_length=1),
        "email": fields.String(required=True, min_length=5),
        "phone_no": fields.String(required=False, min_length=5),
    },
)

parser = reqparse.RequestParser()
parser.add_argument("sort", type=str, action="split")


@ns.route("/user/")
class UserList(Resource):
    """Get a list of all users, and POST a new user"""

    @ns.doc("list_users")
    @ns.marshal_list_with(user)
    def get(self):
        """List all users"""
        sort = parser.parse_args()["sort"]
        user_schema = UserSchema(many=True)
        users = user_schema.dump(User.query.all())
        return users

    @ns.doc("create_user")
    @ns.expect(user)
    @ns.marshal_list_with(user, code=201)
    def post(self):
        """Create a new user"""
        input = request.json
        user_schema = UserSchema()
        # user = user_schema.load(data)
        user = User(name=input["name"], email=input["email"])
        user.create()
        user.commit()
        output = user_schema.dump(user)
        return output, 200


@ns.route("/user/<int:id>")
@ns.response(404, "User ")
class UserItem(Resource):
    """Get, Delete and Put a user"""

    @ns.doc("get_user")
    @ns.marshal_with(user)
    def get(self, id):
        """Fetch a given its identifier"""
        user = User.query.filter_by(id=id).first_or_404()
        return user

    @ns.doc("delete_user")
    @ns.response(204, "User is deleted")
    def delete(self, id):
        """Delete a user given its identifier"""
        user = User.query.filter_by(id=id)
        user.delete()
        user.commit()
        return "", 204

    @ns.doc("update_user")
    @ns.marshal_with(user)
    def put(self, id):
        """Update a user given its identifier"""
        input = request.json
        user_schema = UserSchema()
        user = User.query.filter_by(id=id)
        user.name = input["name"]
        user.email = input["email"]
        user.commit()
        output = user_schema.dump(user)
        return output, 200
