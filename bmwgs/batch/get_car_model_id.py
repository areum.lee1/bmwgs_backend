import csv, re


# 정규식을 이용해 contents에 들어있는 텍스트가 몇 시리즈에 해당하는지 판단해주는 function입니다.

# 2021/11/18 - 수정내용 : 정규식 표현이  'r[m,M]{0,1}1[1-9]..[R,r]'-> r'[m,M]{0,1}1[1-9]..[R,r]'수정
# 형식에 첫문장에 r을 포함하고 있어서, 대부분의 필터링이 제외 된거 같습니다. 
def get_car_model_id(line): 
    series_list = [
    'r[m,M]{0,1}1[1-9][0-9][d,D,i,I]|1.{0,1}시리즈|1[s,S][e,E][R,r]',  #'0번이 나오지 않게 하기 위함'
    r'[m,M]{0,1}1[1-9][0-9][d,D,i,I]|1.{0,1}시리즈|1[s,S][e,E][R,r]',  #'series1'
    r'[m,M]{0,1}2[1-9][0-9][a-zA-Z]|2.{0,1}시리즈|2[s,S][e,E][R,r]',  #'series2'
    r'[m,M]{0,1}3[1-9][0-9][a-zA-Z]|3.{0,1}시리즈|3[g,G][t,T]',  #'series3'
    r'[m,M]{0,1}4[1-9][0-9][a-zA-Z]|4.{0,1}시리즈',  #'series4'
    r'[m,M]{0,1}5[1-9][0-9][a-zA-Z]|5.{0,1}시리즈|5[g,G][t,T]|5[s,S][e,E][R,r]',  #'series5'
    r'[m,M]{0,1}6[1-9][0-9][a-zA-Z]|6.{0,1}시리즈|6[g,G][t,T]|5[s,S][e,E][R,r]',  #'series6'
    r'[m,M]{0,1}7[1-9][0-9][a-zA-Z]|7.{0,1}시리즈|7[s,S][e,E][R,r]|[m,M]{0,1}7[1-9][0-9].{0,1}그란',  #'series7'
    r'[m,M]{0,1}8[1-9][0-9][a-zA-Z]|8.{0,1}시리즈|[m,M]{0,1}8[1-9][0-9].{0,1}그란',  #'series8'
    r'[i,I]3',  # 'i3'
    r'[i,I][x,X]|아이엑스',  # 'iX'
    r'[m,M]{1}2[^\w]',  #'mseries2'
    r'[m,M]{1}3[^\w]',  #'mseries3'
    r'[m,M]{1}4[^\w]',  #'mseries4'
    r'[m,M]{1}5[^\w]',  #'mseries5'
    r'[m,M]{1}8[^\w]',  #'mseries8'
    r'[z,Z]{1}4[^\w]',  #'zeries'
    r'[x,X]1[^m,M]',  #'x1'
    r'[x,X]2[^m,M]',  #'x2'
    r'[x,X]3[^m,M]',  #'x3'
    r'[x,X]4[^m,M]',  #'x4'
    r'[x,X]5[^m,M]',  #'x5'
    r'[x,X]6[^m,M]',  #'x6'
    r'[x,X]7[^m,M]'  #'x7'
    r'[x,X]3[m,M]',  #'x3m'
    r'[x,X]4[m,M]',  #'x4m'
    r'[x,X]5[m,M]',  #'x5m'
    r'[x,X]6[m,M]',  #'x6m'
    ]

    for i in range(1, len(series_list)):
        pattern = series_list[i]
        r = re.search(pattern, line)
        if r != None:
            return i
    return None

# csv불러오는 부분 d2.csv는 아래와 같은 형태로 되어 있습니다.
'''
id,cs_id,status,cs_start_date,cs_category_id,contents
89682,7447840,COMPLETE,2021.10.29,5,TF00491 고객인입 정승원님 통화 엔진오일 브레이크오일 경고등점등 바바리안 미니인천계양센터 요청 dss 2021 11 15 09 00 dss 예약완료 픽딜요청 부천시 중동로 107 팰리스카운티 128동 cic기재완료 고객에게는 접수사항 센터에서 확인후 스케쥴 조정 필요시 고객님께 연락드릴 수 있는점 안내
89683,7448060,COMPLETE,2021.10.29,15,VANTAGE 일반문의 탈퇴요청 15분전에 하셨다고함 왜 로그인되냐고 말씀하시어 시간 최소2주 소요 됨을 안내 수긍
89684,7447450,COMPLETE,2021.10.29,5,캠페인 문자인입 연장패키지 구매 방법 문의 서비스센터통해 11 1일부터 할인 가격으로 구매가능안내
'''
# f = open('../../data_preparing/d3.csv', r'', encoding='utf-8')
# rdr = csv.reader(f)
#
# cnt = 0
# rdrlist = list(rdr)
# for line in rdrlist:
#     r = get_car_model_id(line[5])
#     if r != None:
#         cnt += 1
#         print(r, line[5])
# print('cnt:', cnt, len(rdrlist))
# f.close()
