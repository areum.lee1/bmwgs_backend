from flask_restx import Namespace

ns = Namespace("Category", path="/category")

from bmwgs.category.v1 import views  # noqa
