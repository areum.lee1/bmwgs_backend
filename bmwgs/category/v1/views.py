from flask import request
from flask_restx import fields
from flask_restx import reqparse
from flask_restx import Resource

from bmwgs.category import ns
from bmwgs.models.category import CustomerServiceCategory
from bmwgs.models.category import CustomerServiceCategorySchema

parser = reqparse.RequestParser()
category_schema = CustomerServiceCategorySchema()
categories_schema = CustomerServiceCategorySchema(many=True)

category_response_dto = ns.model(
    "Category",
    {
        "parent_id": fields.Integer,
        "level": fields.Integer,
        "category_name": fields.String(required=True),
    },
)


@ns.route("/")
class GetCustomerServiceCategoryList(Resource):
    """ 모든 카테고리 리스트를 반환한다. """

    def get(self):
        """모든 카테고리 리스트를 반환한다."""
        root_categories = CustomerServiceCategory. \
            get_customer_service_category_by_parent_is_null()
        return categories_schema.dump(
            root_categories
        ), 200

    create_category_request_dto = ns.model(
        "Category",
        {
            "parent_id": fields.Integer,
            "level": fields.Integer,
            "category_name": fields.String(required=True),
        },
    )

    create_category_response_dto = ns.model(
        "Category",
        {
            "parent_id": fields.Integer,
            "id": fields.Integer,
            "category_name": fields.String(required=True),
        }
    )

    @ns.doc("create category")
    @ns.expect(create_category_request_dto)
    @ns.marshal_list_with(create_category_response_dto, code=201)
    def post(self):
        """ 카테고리를 생성한다. """
        params = request.json

        customer_service_category = CustomerServiceCategory(
            parent_id=params["parent_id"],
            level=params["level"],
            category_name=params["category_name"]
        )

        customer_service_category.create()
        customer_service_category.commit()
        return category_schema.dump(customer_service_category), 201


@ns.route("/<int:category_id>")
class GetCustomerServiceCategory(Resource):
    """ 카테고리를 반환한다."""

    @ns.doc("create category")
    @ns.marshal_list_with(category_response_dto, code=200)
    def get(self, category_id):
        """카테고리를 반환한다."""
        category = CustomerServiceCategory \
            .get_customer_service_category_by_id(category_id)
        return categories_schema.dump(
            category
        ), 200




@ns.route("/<int:category_id>", methods=["PUT"])
class UpdateCustomerServiceCategory(Resource):
    """ 카테고리를 수정한다. """
    update_category_request_dto = ns.model(
        "Category",
        {
            "parent_id": fields.Integer,
            "level": fields.Integer,
            "category_name": fields.String(required=True),
        },
    )

    @ns.doc("카테고리를 수정")
    @ns.expect(update_category_request_dto)
    @ns.marshal_list_with(category_response_dto, code=200)
    def put(self, category_id):
        """
        카테고리를 수정한다.
        """
        params = request.json

        target_customer_service = CustomerServiceCategory(
            parent_id=params["parent_id"],
            level=params["level"],
            category_name=params["category_name"]
        )

        category = CustomerServiceCategory.query.get(category_id)
        category.change_with(target_customer_service)
        return category_schema.dump(category)


@ns.route("/<int:category_id>", methods=["DELETE"])
class DeleteCustomerServiceCategory(Resource):
    """
    카테고리를 삭제한다.
    """
    category_response_dto = ns.model(
        "Category",
        {
            "parent_id": fields.Integer,
            "level": fields.Integer,
            "category_name": fields.String(required=True),
        },
    )

    @ns.doc("카테고리를 삭제")
    @ns.marshal_list_with(category_response_dto, code=204)
    def delete(self, category_id):
        """
        개별 차량 모델 정보를 삭제한다.
        """
        category = CustomerServiceCategory.query.get(category_id)
        return category.delete(), 204
