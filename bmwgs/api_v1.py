from flask import Blueprint, current_app
from flask_restx import Api

from bmwgs.statistics import ns as ns_statistics
from bmwgs.csrecord import ns as ns_csrecord
from bmwgs.category import ns as ns_category
from bmwgs.keyword import ns as ns_keyword
from bmwgs.car import ns as ns_car

blueprint = Blueprint("api_1_0", __name__)


api = Api(blueprint, doc=current_app.config["API_DOCS_URL"], catch_all_404s=True)
api.namespaces.clear()
api.add_namespace(ns_csrecord)
api.add_namespace(ns_category)
api.add_namespace(ns_keyword)
api.add_namespace(ns_car)
